<?php get_header(); ?>

<article id="contenido" class="home">
    <div class="container">
        <?php 
        if ( have_posts() ) { 
            $i = 1;
            while ( have_posts() ) { 
                the_post();
                if ($i == 1){
                    echo ' <div class="row" style="margin-top: 30px; margin-bottom: 30px">';
                }
            ?>
    	    <div class="col-md-6">
        	    <a href="<?php the_permalink(); ?>" class="snippet-noticias">
    	    	    <div class="foto">
        	    	    <div class="<?php echo ($i % 2 == 0) ? 'corte-derecha-arriba' : 'corte-izquierda-arriba'; ?>"></div>
    	    	        <img src="<?php the_post_thumbnail_url( 'ficha' ); ?>" srcset="<?php echo wp_get_attachment_image_sizes('ficha'); ?>" alt="" class="img-responsive" />
    	    	    </div>
    	    	    <h2 class="text-uppercase"><?php the_title(); ?></h2>
    	    	    <p class="fecha"><?php echo get_the_date('d/m/Y'); ?></p>
    	    	    
<!--
                                <p>
                                    
                                    <?php 
                                        foreach((get_the_category()) as $category) { 
                                            echo $category->cat_name;
                                        } 
                                    ?>
                                </p>
-->
    	    	    
                </a>
    	    </div><!-- .col-md-6 -->
            <?php 
                if ($i == 2) { 
                    echo '</div> ';
                    $i = 1;
                }else {
                    $i++;
                }
                
            } 
        } wp_reset_postdata();
        if ($i == 1){
            echo '</div> ';
        }
        ?>
    </div><!-- container -->
        
    <div class="container" style="padding-bottom: 50px;>
        <div class="row">
            <div class="col-md-6">
                <?php previous_posts_link(); ?>
            </div>
            <div class="col-md-6 text-right">
                <?php next_posts_link(); ?>
            </div>
        </div>
    </div>
</article>
	
<?php #get_sidebar(); ?>
<?php get_footer(); ?>