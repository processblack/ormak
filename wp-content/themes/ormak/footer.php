        <footer id="pie">
            <div class="container">
            	<div class="row">
            	    <div class="col-md-1">
            	    	<img src="<?php bloginfo( 'template_url' ); ?>/img/logo.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/logo@2x.png 2x" alt="<?php bloginfo('title');?>" class="img-responsive center-block">
            	    </div><!-- .col-md-1 -->
            	    
            	    <div class="col-md-3 col-md-offset-1">
                	    <p>
                	    	<?php echo __('Polígono Belartza<br />
                            C/Fernando Múgica, 11 - 2.a<br />
                            20018 Donostia', 'ETG_text_domain'); ?>
                        </p>
            	    </div><!-- .col-md-3 -->
            	    
            	    <div class="col-md-3">
                        <?php if (ICL_LANGUAGE_CODE == 'es') { 
                            wp_nav_menu( array('menu' => 'Pie', 'container' => false));
                        } else {
                            wp_nav_menu( array('menu' => 'Pie - Euskera', 'container' => false ));
                        }
                        ?>
            	    </div><!-- .col-md-3 -->
            	    <div class="col-md-3">
            	    	<p>
                	    	943 424 430<br />
                            <a href="mailto:ormak@ormak.es">ormak@ormak.es</a>
                        </p>
            	    </div><!-- .col-md-3 -->
            	</div><!-- .row -->
            </div><!-- .container -->
        </footer>

<?php if (!isset($_COOKIE['aceptar_cookies']) or $_COOKIE['aceptar_cookies'] !== 'si'){ ?>
<div id="aviso-cookies">
    <p class="text-center"><?php echo __('En nuestra web utilizamos cookies propias y de terceros para ofrecerte un mejor servicio cuando vuelvas a visitarnos. Al navegar por nuestra web estás aceptando su uso. Conoce <a href="/es/politica-de-cookies">aquí</a> toda la información sobre nuestra política de cookies', 'ETG_text_domain'); ?></p>
    <p class="text-center"><a href="#" id="aceptar-cookies" class="btn"><?php echo __('Aceptar', 'ETG_text_domain'); ?></a>
</div>
<?php } ?>
        
        
        <div id="buscador">
            <form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
                <div class="centrador">
                <p><input type="text" class="field" name="s" id="s"  placeholder="" /></p>
                <input type="submit" class="submit" name="submit" value="Buscar" />
                <a href="javascript:cerrarventana('buscador');" class="cerrar-modal" id="boton_buscador_cerrar"><img src="<?php bloginfo( 'template_url' ); ?>/img/cerrar.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/cerrar@2x.png 2x" alt="<?php echo __('Cerrar', 'ETG_text_domain'); ?>"></a>
                <p class="hidden">
                <input type="radio" name="area" id="buscador_agenda" value="agenda" /> 
                <label for="buscador_agenda">Agenda</label> 
                <input type="radio" name="area" id="buscador_todo" value="todo" /> 
                </p>
                </div>
            </form>
        </div>
        

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
        <script src="<?php bloginfo('template_url');?>/js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
        <script src="<?php bloginfo('template_url');?>/js/plugins.js"></script>
        <script src="<?php bloginfo('template_url');?>/js/main.js"></script>
        
        
        <?php wp_footer(); ?>
    </body>
</html>