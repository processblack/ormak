<?php 
    // Template Name: Cómo lo hacemos
    get_header();
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <article id="contenido" class="pagina">
        <?php 
            ETG_cabecera(get_the_ID());
        ?>
        <div class="container">
        	<div class="row">
        	    <div class="col-md-8 col-md-offset-2">
                    <h2 class="titular visible-xs"><?php the_title();?></h2>
            		<?php the_content();?>      
        	    </div><!-- .col-md-8 -->
        	</div><!-- .row -->
        </div><!-- .container -->
        
        <?php
        if( have_rows('crear_contenido') ):
            while ( have_rows('crear_contenido') ) : the_row();
                if( get_row_layout() == 'banda_de_foto' ):
                    get_template_part('contenidos/banda-de-foto');
                
                elseif( get_row_layout() == 'banda_de_texto' ): 
                    get_template_part('contenidos/banda-de-texto');

                elseif( get_row_layout() == 'tres_columnas' ): 
                    get_template_part('contenidos/tres-columnas');
                    
                endif;
            endwhile;
        endif;
        ?>
        
        <div style="background:#949a90; padding: 20px 0;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <?php if ($idioma == 'eu') { ?>
                            <div class="embed-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/WIIcsmZNmJk?rel=0&showinfo=0&autohide=1" frameborder="0" allowfullscreen></iframe></div>
                            <?php } else { ?>
                            <div class="embed-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/eXWFe78RROc?rel=0&showinfo=0&autohide=1" frameborder="0" allowfullscreen></iframe></div>
                            <?php } ?>
                        </div><!-- .col-md-12 -->
                    </div><!-- .row -->
                </div><!-- container -->
            </div>
        
        
        
        <?php 
            if (get_field('titular')){
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <br><h2 class="text-center text-uppercase">
                        <?php echo get_field('titular'); ?>
                    </h2>
                </div>
            </div>
        </div>
        <?php } ?>
        
        
        <div class="banda-color pd50">
            <div class="container">
                <?php 
                    if( have_rows('bloque')) {
                        $i = 1;
                        while ( have_rows('bloque') ) : the_row();
                            if ($i == 1 or $i == 3 or $i == 5){
                                echo '<div class="row pd30">';
                            }
                            $foto = get_sub_field('icono');
                            $urlFoto = $foto['url'];
                            if ($i == 5){
                                $classSnippet = 'col-md-6 col-md-offset-3';
                            } else {
                                $classSnippet = 'col-md-6';
                            }
    
                            ?>
                            <div class="<?php echo $classSnippet; ?>">
                        	    <p><img src="<?php echo $urlFoto; ?>" alt="<?php the_sub_field('titular'); ?>"></p>
                    	    	<h3 class="text-uppercase"><?php the_sub_field('titular'); ?></h3>
                    	    	<p><?php the_sub_field('texto'); ?></p>
                    	    </div><!-- .col-md-6 -->                        
                            <?php
                            if ($i == 2 or $i == 4 or $i == 5){
                                echo '</div><!-- row -->';
                            }
                            $i++;
                        endwhile;
                    }                
                ?>
            </div><!-- .container -->
        </div>        
        
        
        
        
<!--
        <div class="banda-color pd50">
        
        <div id="grafic" class="container">
            
        	<div class="row pd30">
        	    <div class="col-md-6">
            	    <p><img src="<?php bloginfo( 'template_url' ); ?>/img/01-ahorro-costes.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/01-ahorro-costes@2x.png 2x" alt="AHORRO EN COSTES"></p>
        	    	<h3><?php echo __('AHORRO EN COSTES', 'ETG_text_domain'); ?></h3>
        	    	<p><?php echo __('Apoyados en nuestra dilatada experiencia y nuestra oficina técnica, aportamos valor a cada uno de los proyectos en los que intervenimos optimizando soluciones técnicas y ejecutivas, que se traducen en competitividad y ahorro en costes.', 'ETG_text_domain'); ?> </p>
        	    </div>
        	    <div class="col-md-6">
            	    <p><img src="<?php bloginfo( 'template_url' ); ?>/img/02-plazos.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/02-plazos@2x.png 2x" alt="CUMPLIMIENTO DE PLAZOS"></p>
        	    	<h3><?php echo __('CUMPLIMIENTO DE PLAZOS', 'ETG_text_domain'); ?></h3>
        	    	<p><?php echo __('Trabajar con ORMAK como responsable del proyecto es GARANTIA de cumplimiento de plazos y presupuestos acordados. Ese es nuestro COMPROMISO', 'ETG_text_domain'); ?></p>
        	    </div>
        	</div>
            <div class="row pd30">
        	    <div class="col-md-6">
            	    <p><img src="<?php bloginfo( 'template_url' ); ?>/img/03-acabados.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/03-acabados@2x.png 2x" alt="GARANTIA DE CALIDAD Y ACABADOS"></p>
        	    	<h3><?php echo __('GARANTIA DE CALIDAD Y ACABADOS', 'ETG_text_domain'); ?></h3>
        	    	<p><?php echo __('Las más de 400 obras ejecutadas por Ormak a lo largo de su larga y dilatada trayectoria junto con la profesionalidad, seriedad y compromiso de su contrastado equipo humano son la mejor garantía para su proyecto.', 'ETG_text_domain'); ?></p>
        	    </div>
        	    <div class="col-md-6">
            	    <p><img src="<?php bloginfo( 'template_url' ); ?>/img/04.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/04@2x.png 2x" alt="FLEXIBILIDAD Y ADAPTACION A LAS NECESIDADES DEL CLIENTE"></p>
        	    	<h3><?php echo __('FLEXIBILIDAD Y ADAPTACION A LAS NECESIDADES DEL CLIENTE', 'ETG_text_domain'); ?></h3>
        	    	<p><?php echo __('Un trato directo y cercano, unido a un sistema de gestión y de obra sencillo y transparente permite introducir cambios en el proyecto en cualquier momento, dotando a los proyectos de la flexibilidad que requieren.', 'ETG_text_domain'); ?></p>
        	    </div>                
            </div>
            <div class="row pd30">
                <div class="col-md-6 col-md-offset-3">
            	    <p><img src="<?php bloginfo( 'template_url' ); ?>/img/05.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/05@2x.png 2x" alt="INTERLOCUCION Y RESPONSABILIDAD UNICA"></p>
        	    	<h3><?php echo __('INTERLOCUCION Y RESPONSABILIDAD UNICA', 'ETG_text_domain'); ?></h3>
        	    	<p><?php echo __('ORMAK se ocupa y responsabiliza del proyecto de principio a fin; garantizando el cumplimiento del presupuesto, plazos, y calidades acordados.', 'ETG_text_domain'); ?></p>
                </div>
            </div>
        </div>
        </div>        
-->
        
    </article>
<?php endwhile; ?>
<?php get_footer(); ?>