<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <article id="contenido" class="pagina">
        <?php 
            ETG_cabecera(get_the_ID());
        ?>
        <div class="container">
        	<div class="row">
        	    <div class="col-md-8 col-md-offset-2">
                    <h2 class="titular visible-xs text-light"><?php the_title();?></h2>
            		<?php the_content();?>      
        	    </div><!-- .col-md-8 -->
        	</div><!-- .row -->
        </div><!-- .container -->
        
        <?php
        if( have_rows('crear_contenido') ):
            while ( have_rows('crear_contenido') ) : the_row();
                if( get_row_layout() == 'banda_de_foto' ):
                    get_template_part('contenidos/banda-de-foto');
                
                elseif( get_row_layout() == 'banda_de_texto' ): 
                    get_template_part('contenidos/banda-de-texto');

                elseif( get_row_layout() == 'tres_columnas' ): 
                    get_template_part('contenidos/tres-columnas');
                    
                endif;
            endwhile;
        endif;
        ?>
    </article>
<?php endwhile; ?>
<?php get_footer(); ?>
