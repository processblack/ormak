<?php get_header(); ?>

<div class="container">
    <article id="contenido" class="archive">
        <?php 
        if ( have_posts() ) { 
            $i = 1;
            while ( have_posts() ) { 
                the_post();
                if ($i == 1){
                    echo '<div class="row">';
                }
            ?>
            
                <div class="col-md-4 col-sm-6 snippet noticias alto-fijo">
                    <?php 
                    if(has_post_thumbnail($the_query->ID)){ ?>
                        <p class="foto">
                            <?php if (kd_mfi_the_featured_image('post-bn', 'post', get_the_id() )) { ?>                                        
                            <span class="bn"><?php echo get_the_post_thumbnail(get_the_id(), 'featured-image-bn', array('class' => 'img-responsive')); ?></span>
                            <span class="screen"><?php echo get_the_post_thumbnail(get_the_id(), 'featured-image-screen', array('class' => 'img-responsive')); ?></span>
                            <?php } else { ?>
                            <a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail('thumbnail', array('class' => 'img-responsive')); ?></a>
                            <?php } ?>
                        </p>
                    <?php 
                    }
                    ?>
            		<ul class="categorias">
                		<li class="fecha"><?php echo get_the_date('d\/m\/Y'); ?></li>
                		<?php echo ETG_categorias(get_the_id()); ?>
                    </ul>
                    
            		<div class="info">
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <p class="martel"><?php echo get_the_excerpt(__('(more…)')); ?></p>
                        <p><a href="<?php echo the_permalink(); ?>"><?php echo __('Leer más...', 'ETG_text_domain'); ?></a></p>
                    </div>
                </div><!-- .col-md-4 -->
            <?php 

                $i++;
                if ($i == 4){
                    $i = 1;
                    echo '</div>';
                }
            } 
        } wp_reset_postdata(); 
        if ($i > 3){
            echo '</div>';
        }
        ?>
	</article>
</div>
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>