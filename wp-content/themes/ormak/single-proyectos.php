<?php get_header(); ?>

<article id="contenido" class="proyecto">

	<div class="fondo-color">
    	<div class="container">
        	<div class="row">
        	    <div class="col-sm-12">
        	    	<div id="slide">
                        <div id="slide_home" class="cycle-slideshow" 
                            data-cycle-fx="fade" 
                            data-cycle-timeout="4000"
                            data-cycle-slides="> div"
                            data-cycle-log="false"
                            data-cycle-pause-on-hover="true"
                            data-cycle-pager="#bullets"
                            data-cycle-pager-template="<strong><a href=#><span>{{slideNum}}<span></a> </strong>"
                            >
                                
                			<?php 
                    		foreach (get_field('slides') as $slide){
                            ?>
                			<div class="slide">
                                <a href="<?php echo $slide['link']; ?>" class="item">
                    			    <div class="corte <?php echo $slide['corte']; ?>"></div>
                                    <img src="<?php echo $slide['fotografia']['sizes']['slide']; ?>" alt="<?php echo $slide['titular_fotografia']; ?>" class="img-responsive" /><div class="caption">
                                    </div>
                                </a>
                            </div>
                    		<?php 
                            }	
                            ?>         
                        </div><!-- #slide_home -->      
                         	
                    </div>
        	    </div><!-- .col-sm-12 -->
        	</div><!-- .row -->
        </div><!-- container -->
    </div>	
    
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
            	<div id="bullets" class="clearfix"></div>
            </div><!-- .col-sm-12 -->
        </div><!-- .row -->
    </div><!-- container -->
    
    
    
    
    <div class="container">
        <?php 
        global $wp_query;
        $args = array_merge( $wp_query->query_vars, array( 'orderby' => 'menu_order',
                                                            'order'  => 'asc'
                                                    )
        );
        query_posts( $args );
            if ( have_posts() ) { 
            $i = 0;
            while ( have_posts() ) { 
                the_post();
            ?>
            
            <div class="row">
                <div class="col-md-12">
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                </div><!-- .col-md-12 -->
            </div><!-- .row -->
            <div class="row">
                <div class="col-md-8">
                    <div class="post">
                		<div class="info">
                        <?php if (get_field('singularidades')){ ?>
                        <div class="entradilla">
                            <?php echo get_field('singularidades'); ?>
                        </div>
                        <?php } ?>
                            <p class="martel"><?php echo the_content(); ?></p>  
                            <?php ETG_galeria(get_the_ID()); ?>
                           
                        </div>
                    </div>                    
                </div><!-- .col-md-8 -->
                <div class="col-md-4" style="padding-top: 10px">                  
                    <?php if (get_field('fecha')){ ?>
                    <p>
                        <strong style="text-transform: uppercase"><?php echo __('Fechas', 'ETG_text_domain'); ?></strong>:<br />
                        <?php echo get_field('fecha'); ?>
                    </p>
                    <?php } ?>
                    <?php if (get_field('autor_del_proyecto')){ ?>
                    <p>
                        <strong style="text-transform: uppercase"><?php echo __('Autor del proyecto', 'ETG_text_domain'); ?></strong>:<br />
                        <?php echo get_field('autor_del_proyecto'); ?>
                    </p>
                    <?php } ?>
                    <?php if (get_field('promotor')){ ?>
                    <p>
                        <strong style="text-transform: uppercase"><?php echo __('Promotor', 'ETG_text_domain'); ?></strong>:<br />
                        <?php echo get_field('promotor'); ?>
                    </p>
                    <?php } ?>
                    <?php if (get_field('presupuesto')){ ?>
                    <p>
                        <strong style="text-transform: uppercase"><?php echo __('Presupuesto', 'ETG_text_domain'); ?></strong>:<br />
                        <?php echo get_field('presupuesto'); ?>
                    </p>
                    <?php } ?>
                </div><!-- .col-md-4 -->
            </div><!-- .row -->
            
            
            <?php 
                $i++;
            } 
        } wp_reset_postdata(); ?>
	</article>
</div>

<?php get_footer(); ?>
