<?php 
    // Template Name: Página de contacto
    $GLOBALS['inicializarMapa'] = true;
    get_header();
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <article id="contenido" class="contacto">
    <div id="map"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
            		<h2 class="titular"><?php the_title();?></h2>
                </div>
                <div class="col-md-6">
                    <p style="margin-top: 10px;" class="text-right">
                        <a href="javascript:comoLlegar();" class="btn btn-primary"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo __('Cómo llegar', 'ETG_text_domain'); ?></a>
                        <span id="enlace_gmaps"></span>
                    </p>
                </div><!-- .col-md-7 -->
            </div><!-- .row -->
            <div class="row">
                <div class="col-md-5">
                	<?php the_content();?>
                </div><!-- .col-md-5 -->
                <div class="col-md-7">
                	<form id="form1" name="form1" method="post" action="" class="form-horizontal">
                		<p class="clearfix form-group">
                			<label for="nombre"><?php echo __('Nombre', 'ETG_text_domain'); ?></label>
                			<input type="text" name="nombre" id="nombre" class="form-control" />
                		</p>
                		<p class="clearfix form-group">
                			<label for="telefono"><?php echo __('Teléfono', 'ETG_text_domain'); ?></label>
                			<input type="text" name="telefono" id="telefono" class="form-control" />
                		</p>
                		<p class="clearfix form-group">
                			<label for="mail"><?php echo __('E-mail', 'ETG_text_domain'); ?></label>
                			<input type="text" name="mail" id="mail" class="form-control" />
                		</p>
                		<p class="clearfix form-group">
                			<label for="mensaje"><?php echo __('Comentario', 'ETG_text_domain'); ?></label>
                			<textarea name="mensaje" id="mensaje" rows="10" cols="40" class="form-control"></textarea>
                		</p>
                		<p class="empujar form-group">
                			<input type="submit" name="enviar" id="enviar_formulario" value="<?php echo __('Enviar', 'ETG_text_domain'); ?>" class="btn btn-primary" />
                		</p>
                		<div id="resultadoMensaje"></div>
                	</form>
                </div><!-- .col-md-7 -->
            </div><!-- .row -->
        </div><!-- .container -->
    </article>
<?php endwhile; ?>
<?php get_footer(); ?>
