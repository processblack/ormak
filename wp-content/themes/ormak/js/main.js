function ajustar() {
    if ($(window).width() < 992) {
        $(".container").attr('class', 'container-fluid');
    } else {
        $(".container-fluid").attr('class', 'container');
    }
}

$(window).resize(function() {
    ajustar();  
});
$(window).load(function() {
    
    /* 
        Cookies
    */    
        function setCookie(cname, cvalue, exdays){
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            $('#capa_cookies').fadeOut('fast');
        }
        
        $('h1').click(function(){
            console.log('h1');    
        });
        console.log($('#capa_cookies'));
        $('#aceptar-cookies').click(function(e){
            console.log('cookie');
            setCookie('aceptar_cookies', 'si', 5000);
            //$('#aviso-cookies').css('bottom', '40px').fadeOut('slow');
            $('#aviso-cookies').animate({opacity: 0, bottom: '-20px'}, 500).fadeOut('slow');
            e.preventDefault();
        });
   
    
    ajustar();
});




$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
})

$(".boxer").boxer();


$("#menu ul").clone().appendTo($("#menu-movil"));


    $('#boton-buscador').click(function(){
        $('#buscador').fadeIn();
        $('#buscador .centrador *').delay(500).fadeIn(function(){
            $( "#s" ).focus();
        });
    });
    
    $('#control-menu').click(function(){
        $('#menu-movil').addClass('on', {duration:500});
    });
    $('#menu-movil .close').click(function(){
        $('#menu-movil').removeClass('on', {duration:500});
    });

    function cerrarventana(target) {
        $('#'+target).fadeOut();
        $('#'+target+'.centrador *').fadeOut();
    }   
/* 
    - - - - - - - - - - - - - - - - - - - - - - - - 
    Envía formularios
    - - - - - - - - - - - - - - - - - - - - - - - - 
*/
    $('#enviar_formulario').click(function(event) {
        $('#resultadoMensaje').empty();
        $('#resultadoMensaje').append('<p class="gris">Procesando mensaje</p>');
        
        var form_data = {
        // get the form values
            viaje   : $('#viaje').val(),
            nombre  : $('#nombre').val(),
            email   : $('#email').val(),
            telefono: $('#telefono').val(),
            mensaje : $('#mensaje').val(),
            empresa : $('#empresa').val()
        };

        // send the form data to the controller
        $.ajax({
            url: "/enviar-formulario-contacto.php",
            type: "post",
            data: form_data,
            dataType: "json",
            cache: false,
            success: function(msg)
            {
                if(msg.validate) {
                   //console.log("ok");
                   $('#resultadoMensaje').fadeIn();
                   $('#resultadoMensaje').empty();
                   $('#resultadoMensaje').append('<div class="gris"><p>Mensaje enviado!<br />En breve nos pondremos en contacto con usted.</p><p>Un saludo y gracias.</p>');
                   $('form#contacto').fadeOut();
                   $('#enviar_formulario').attr('disabled', 'disabled');
                   //$('#myModal').modal('hide');
                } else {
                   //console.log(msg.mensaje);
                   $('#resultadoMensaje').append('<p class="gris">Error al enviar el mensaje!<br />'+ msg.mensaje +'</p>');
                   $('#resultadoMensaje').fadeIn();
                }
            }
        });
        event.preventDefault();
        // prevents from refreshing the page
    });    
    
    
    
    

/* 
    - - - - - - - - - - - - - - - - - - - - - - - - 
    Google Maps 
    - - - - - - - - - - - - - - - - - - - - - - - - 
*/

// var directionsService = new google.maps.DirectionsService();
var directionDisplay;
var map;
var pos;
var etg_textdomain_position;

/* 
- - - - - - - - - - - - - - - - - - - - - - - - 
Google Maps 
- - - - - - - - - - - - - - - - - - - - - - - - 
*/

// var directionsService = new google.maps.DirectionsService();
var directionsService;
var directionDisplay;
var map;
var pos;
var etg_textdomain_position;

function initialize() {
directionsService = new google.maps.DirectionsService();
console.log(directionsService);
directionsDisplay = new google.maps.DirectionsRenderer();
var myOptions = {
    zoom: 15,
    center: new google.maps.LatLng(43.28121188614717, -2.013055359256896),
    scrollwheel: false,
    navigationControl: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP, 
    styles: [{"stylers":[{"saturation":-100},{"gamma":1}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"on"},{"saturation":50},{"gamma":0},{"hue":"#50a5d1"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#333333"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"weight":0.5},{"color":"#333333"}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"gamma":1},{"saturation":50}]}]
}
map = new google.maps.Map(
document.getElementById("map"),
myOptions);
directionsDisplay.setMap(map);
directionsDisplay.setPanel(document.getElementById("directionsPanel"));
//setMarkers(map, 43.324954946678154, -1.983727455171902);
etg_textdomain_position = new google.maps.LatLng(43.28121188614717, -2.013055359256896);
var marker = new google.maps.Marker({
    position: etg_textdomain_position,
    map: map,
    //icon: '/img/logo-gmap.png',
    title: 'Jeiber',
    zIndex: 1
});
}

function comoLlegar() {
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
        pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        var user = new google.maps.Marker({
            position: pos,
            map: map,
            title: 'Usuario',
            zIndex: 2
        });
        request = {
            origin: pos,
            destination: etg_textdomain_position,
            travelMode: google.maps.DirectionsTravelMode.DRIVING,
            region: 'en-EN'
        };
        if (pos && etg_textdomain_position) {
            console.log(directionsService);
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    document.getElementById('enlace_gmaps').insertAdjacentHTML('beforeend', '<a href="http://maps.google.com/maps?saddr=' + pos + '&amp;daddr=' + etg_textdomain_position + '&amp;ll=' + pos + '" class="btn btn-primary"><span class="glyphicon glyphicon-road" aria-hidden="true"></span>  Abrir en Google Maps</a>');

                }
            });
        }
    });
}
}

