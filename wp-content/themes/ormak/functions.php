<?php
    
function set_idiomas() {
    global $idioma, $locale,$fecha;
    
    $fecha = date('Y-m-d');
    $idioma = ICL_LANGUAGE_CODE;
	switch ($idioma){
		case "es":
            define('MY_LOCALE', 'es_ES.UTF-8');
            define('IDIOMA_AGENDA', 'es');
            define('IDIOMA_NOTICIAS', 'es');
		break;
		case "eu":
            define('MY_LOCALE', 'eu_ES.UTF-8');
            define('IDIOMA_AGENDA', 'eu');
            define('IDIOMA_NOTICIAS', 'eu');
		break;
		case "en":
            define('MY_LOCALE', 'en_EN.UTF-8');
            define('IDIOMA_AGENDA', 'en');
            define('IDIOMA_NOTICIAS', 'es');
		break;
		case "fr":
            define('MY_LOCALE', 'fr_FR.UTF-8');
            define('IDIOMA_AGENDA', 'fr');
            define('IDIOMA_NOTICIAS', 'fr');
		break;
		default:
            define('MY_LOCALE', 'es_ES.UTF-8');
            define('IDIOMA_AGENDA', 'es');
            define('IDIOMA_NOTICIAS', 'es');
		break;
	}
	setlocale(LC_TIME, MY_LOCALE);
    $idioma = ICL_LANGUAGE_CODE;
}
add_action( 'init', 'set_idiomas' ); 

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 360, 360, true );
#add_image_size( 'miniatura', 360, 360, true );
add_image_size( 'ficha', 729,442, true);
add_image_size( 'miniatura-grande', 300, 300, true );
add_image_size( 'slide', 1160, 550, true );




function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );



add_filter( 'embed_oembed_html', 'custom_oembed_filter', 10, 4 ) ;

function custom_oembed_filter($html, $url, $attr, $post_ID) {
    $return = '<div class="video-container">'.$html.'</div>';
    return $return;
}

function ETG_categorias ($id){
    if (get_post_type($id) == "agenda"){
        $tipo = get_the_terms($id, 'tipo');
        if(get_the_terms($id, 'tipo')){
            foreach($tipo as $c) {
            	$cat = get_category($c);
            	#print_r($cat);
            	$li.='<li><a href="'.get_category_link($cat->term_id).'" class="text-uppercase">'.$cat->cat_name.'</a></li>';
            }
        }
        $programa = get_the_terms($id, 'programa');
        if (get_the_terms($id, 'programa')){
        foreach($programa as $c) {
        	$cat = get_category($c);
        	#print_r($cat);
        	$li.='<li><a href="'.get_category_link($cat->term_id).'" class="text-uppercase">'.$cat->cat_name.'</a></li>';
        }
        }
    } else {
        $cont = wp_get_post_categories($id);
        foreach($cont as $c) {
        	$cat = get_category($c);
        	#print_r($cat);
        	$li.='<li><a href="'.get_category_link($cat->term_id).'" class="text-uppercase">'.$cat->cat_name.'</a></li>';
        }
    
    }
    
    return $li;
}


function ETG_galeria($id){
    $images = get_field('galeria_de_fotos', $id);
    
    if($images){ ?>
        <h3>Galería de fotos</h3>
        <div id="galeria" class="row">
            
        <?php foreach( $images as $image ): ?>
        <div class="col-md-12">
<!--             <a href="<?php echo $image['url']; ?>" class="boxer"> -->
                 <p><img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive center-block" /></p>
<!--             </a> -->
            <!-- <p><?php echo $image['caption']; ?></p> -->
        </div><!-- .col-md-4 -->
        <?php endforeach; ?>
        </div><!-- .row -->
    <?php 
    }
}


function ETG_cabecera($id){
    if (have_rows('cabecera', $id)){ ?>
        <div id="imagen_cabecera">
        <?php
        if (count(get_field('cabecera', $id)) == 1){
            # Imagen fija
            while ( have_rows('cabecera', $id) ) : the_row(); ?>
            <a href="<?php the_sub_field('link', $id); ?>" style="background: transparent url(<?php the_sub_field('fotografia', $id); ?>) no-repeat 50%; background-size: cover">
                <div class="titular"><?php the_sub_field('titular', $id); ?></div>
                <div class="texto-apoyo"><?php the_sub_field('texto_de_apoyo', $id); ?></div>
            </a>
            <?php endwhile;
        } else {
            # Carrusel de foto
            ?>
            <div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="2000" data-cycle-slides="> a">
            <?php
            while ( have_rows('cabecera', $id) ) : the_row(); ?>
            <a href="<?php the_sub_field('link', $id); ?>" style="background: transparent url(<?php the_sub_field('fotografia', $id); ?>) no-repeat 50%; background-size: cover">
                <div class="titular"><?php the_sub_field('titular', $id); ?></div>
                <div class="texto-apoyo"><?php the_sub_field('texto_de_apoyo', $id); ?></div>
            </a>
            <?php endwhile;
            ?>
            </div>
            <?php
        }
        ?>
        </div>
        <?php
    } else {
        if (has_post_thumbnail($id)){
            ?>
        <div id="imagen_cabecera">
            <div href="<?php the_sub_field('link', $id); ?>" style="background: transparent url(<?php echo get_the_post_thumbnail_url($id, 'slide'); ?>) no-repeat 50%; background-size: cover; height: 400px;" class="hero">
            </div>
        </div><!-- imagen cabecera -->
            <?php
        }
    }
}


    
# Soporte idiomas     
load_theme_textdomain('ETG_text_domain', get_template_directory() . '/languages');



// ******************* Sidebars ****************** //

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Pages',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
}

// ******************* Add Custom Menus ****************** //

add_theme_support( 'menus' );


// ******************* Post Thumbnails ****************** //


/*
    CUSTOM POST TYPE    
*/
$labels = array(
	'name'               => _x( 'Proyectos', 'post type general name', 'ETG_text_domain' ),
	'singular_name'      => _x( 'Proyecto', 'post type singular name', 'ETG_text_domain' ),
	'menu_name'          => _x( 'Proyectos', 'admin menu', 'ETG_text_domain' ),
	'name_admin_bar'     => _x( 'Proyecto', 'add new on admin bar', 'ETG_text_domain' ),
	'add_new'            => _x( 'Agregar nuevo', 'book', 'ETG_text_domain' ),
	'add_new_item'       => __( 'Agregar nuevo proyecto', 'ETG_text_domain' ),
	'new_item'           => __( 'Nuevo proyecto', 'ETG_text_domain' ),
	'edit_item'          => __( 'Editar proyecto', 'ETG_text_domain' ),
	'view_item'          => __( 'Ver proyecto', 'ETG_text_domain' ),
	'all_items'          => __( 'Todos los proyectos', 'ETG_text_domain' ),
	'search_items'       => __( 'Buscar proyectos', 'ETG_text_domain' ),
	'parent_item_colon'  => __( 'Proyectos hijo:', 'ETG_text_domain' ),
	'not_found'          => __( 'No se encontraron proyectos.', 'ETG_text_domain' ),
	'not_found_in_trash' => __( 'No se encontraron proyectos en la papelera.', 'ETG_text_domain' )
);



register_post_type('proyectos', array(
	'labels' => $labels,
	'public' => true,
	'menu_icon' => 'dashicons-hammer',
	'show_ui' => true,
	'capability_type' => 'post',
	'hierarchical' => true,
	'rewrite' => array( 'slug' => 'proyectos'),
	'query_var' => false,
	'has_archive' => true,
	'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions')
));


add_action( 'init', 'build_taxonomies', 0 );

function build_taxonomies() {
    register_taxonomy( 'como_lo_hacemos', 'proyectos', array( 'hierarchical' => true, 'label' => 'Tipología', 'show_in_nav_menus' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'tipologia' ) ) ); 
    register_taxonomy( 'que_hacemos', 'proyectos', array( 'hierarchical' => true, 'label' => 'Categoría', 'show_in_nav_menus' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'que-hacemos' ) ) ); 
    #register_taxonomy( 'tipo', 'proyectos', array( 'hierarchical' => true, 'label' => 'Tipo', 'show_in_nav_menus' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'tipo' ) ) ); 
}

/*
* Añade estilos personificados a la barra de herramientas de tiny_mce
*/

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'my_mce_buttons_2');

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'grande',  
			'block' => 'h2',  
			'classes' => 'grande',
			'wrapper' => true,
		)
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  
