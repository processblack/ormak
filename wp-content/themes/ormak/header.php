<!doctype html>
<html>
	<head>
	  	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
			
		<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
		
        <!-- Bootstrap -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">
        <link href="<?php bloginfo( 'template_url' ); ?>/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?date=<?php echo date('Hmshs'); ?>"/>
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

		<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

		<?php wp_head(); ?>

	<?php if ($GLOBALS['inicializarMapa']) { ?>
	    <script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true&key=AIzaSyA6CnTSTR1ZT1tMQfnubgKwKmCdW8m4-w0"></script>
	    </head>	
	    <body <?php body_class($color); ?> onload="initialize()">
	<?php } else { ?>
	    </head>	
	    <body <?php body_class($color); ?>>
	<?php } ?>	
	
    <div id="menu-movil"><div class="close"></div></div>	
        
    <div id="contacta">
    	<a href="<?php bloginfo( 'url' ); ?>/contacto">
            <?php echo __('Consulta sin compromiso', 'ETG_text_domain'); ?>
        </a>
    </div>
    
    
    <div id="utiles">
        <div class="container">
        	<div class="col-md-12">
                <ul id="idiomas">
                    <?php 
                    	if (function_exists('icl_get_languages')) {
                        $idiomas = icl_get_languages('skip_missing=0&orderby=KEY&order=DIR&link_empty_to=str');
                    	$nIdiomas = count($idiomas);
                    	$i = 1;
                    	#print_r($idiomas);
                    	foreach ($idiomas as $idioma) {
                        	$idArticulo = $wp_query->get_queried_object_id();
                        	$idArticuloIdioma = icl_object_id($idArticulo, 'agenda', 0,$idioma['language_code']);
                        	if ($idArticuloIdioma == ""){
                            	$idArticuloIdioma = icl_object_id($idArticulo, 'agenda', 0,'es');
                        	}
                        	echo '<!-- ->'.$idArticulo.$idioma['language_code'].$idArticuloIdioma.'-->';
                        	$linkIdioma = get_permalink($idArticuloIdioma);
                            	   ?>
                                   <li><a href="<?php echo ($idioma['url'] == 'str') ? $linkIdioma : $idioma['url']; ?>" title="<?php echo $idioma['native_name']; ?>" <?php if ($idioma['active'] == 1) echo 'class="current"'; ?>><?php echo $idioma['language_code']; ?></a></li>
                            	   <?php
                        ?>
                    <?php 
                            $i++;
                    	}
                    	}
                    ?>
                </ul>
                
                <img id="boton-buscador" src="<?php bloginfo( 'template_url' ); ?>/img/lupa.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/lupa@2x.png 2x" alt="<?php bloginfo('title');?>" class="img-responsive center-block" />
                
                <div id="taco">
<!--                     <a href="mailto:ormak@ormak.es ">ormak@ormak.es</a><span class="hide-xs"> · </span> --><a href="<?php bloginfo( 'url' ); ?>/<?php echo __('contacto', 'ETG_text_domain'); ?>" class="telefono"><?php echo __('Contacto', 'ETG_text_domain'); ?></span>
                </div>

                <div id="control-menu" class="visible-xs"></div>
                
        	</div><!-- .col-md-12 -->
        </div><!-- .container -->
    </div>


    <header id="cabecera">
        <div class="container">
        	<div class="row">
        	    <div class="col-md-2 col-sm-12 col-xs-3">
                    <h1><a href="<?php bloginfo('url');?>"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/logo@2x.png 2x" alt="<?php bloginfo('title');?>: <?php echo __('Tu empresa constructora de confianza en Gipuzkoa.', 'ETG_text_domain'); ?>" class="img-responsive" /></a></h1>
        	    </div><!-- .col-md-2 -->
        	    <div class="col-lg-10 col-md-12">
                    <nav id="menu">
                        <?php wp_nav_menu( array('menu' => 'Principal', 'container' => false, 'menu_class' => 'text-lowercase' )); ?>
                    </nav>
        	    </div><!-- .col-md-10 -->
        	</div><!-- .row -->
        </div><!-- .container -->
    </header>