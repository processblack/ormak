<?php get_header(); ?>

<div class="container">
    <article id="contenido" class="taxonomy">
        
        <div class="dropdown-group">
            <div class="row">
                <!-- <div class="col-md-4">
                    <div class="dropdown">
                        <button class="dropbtn"><?php echo __('Clientes', 'ETG_text_domain'); ?></button>
                        <ul class="dropdown-content nobullet">
                        	<?php
                            $terms = get_terms( array(
                                'taxonomy' => 'como_lo_hacemos',
                                'hide_empty' => 0
                            ));
                            foreach ($terms as $term){
                                echo '<li><a href="'.get_term_link($term->term_id).'">'.$term->name.'</a></li>';
                            }
                            ?>
                        </ul>
                    </div>            	
                </div>.col-md-4 -->
                <!--<div class="col-md-4">
                    <div class="dropdown">
                        <button class="dropbtn"><?php echo __('¿Cómo lo hacemos?', 'ETG_text_domain'); ?></button>
                        <ul class="dropdown-content nobullet">
                        	<?php
                            $terms = get_terms( array(
                                'taxonomy' => 'que_hacemos',
                                'hide_empty' => 0
                            ));
                            foreach ($terms as $term){
                                echo '<li><a href="'.get_term_link($term->term_id).'">'.$term->name.'</a></li>';
                            }
                            ?>
                        </ul>
                    </div>            	
                </div>.col-md-4 -->
            </div><!-- .row -->
        </div>   
        
        <?php 
        $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
        $acf_term = get_queried_object();
        $descripcion_seccion = $term->description;
        ?>
            <div class="row" style="padding-top: 20px; padding-bottom: 40px;">
                <div class="col-md-8">
                	<h2 class="text-uppercase text-light"><?php echo $term->name; ?></h2>
                	<?php if (get_field('descripcion_corta', $acf_term)) {
                    	echo '<p class="descripcion_corta">'.get_field('descripcion_corta', $acf_term).'</p>';
                	} ?>
                </div><!-- .col-md-4 -->
                <div class="col-md-4" style="margin-top: 30px;">
                    <div class="dropdown">
                        <button class="dropbtn"><?php echo __('Tipología', 'ETG_text_domain'); ?></button>
                        <ul class="dropdown-content nobullet">
                        	<?php
                            $terms = get_terms( array(
                                'taxonomy' => 'como_lo_hacemos',
                                'hide_empty' => 0
                            ));
                            foreach ($terms as $term){
                                echo '<li><a href="'.get_term_link($term->term_id).'">'.$term->name.'</a></li>';
                            }
                            ?>
                        </ul>
                    </div>            	
                </div><!-- .col-md-4 -->
            </div>
            
            
<!--
            <div class="row iconos-tipologia">
                	<?php
                    $term_i = 1;
                    $terms = get_terms( array(
                        'taxonomy' => 'como_lo_hacemos',
                        'hide_empty' => 0
                    ));
                    foreach ($terms as $term){
                        //echo '<li><a href="'.get_term_link($term->term_id).'">'.$term->name.'</a></li>';
                        ?>
                	    <div class="col-md-2<?php if ($term_i == 1) { echo ' col-md-offset-1'; } ?>">
                	    	<a href="<?php echo get_term_link($term->term_id) ?>" class="<?php echo $term->slug; ?>">
                    	    	<?php echo $term->name; ?>
                            </a>
                	    </div><!-- .col-md-3 --
                        <?php  
                        $term_i++;
                    }
                    ?>            	
            </div><!-- .row --
            
-->
            
            <div class="row iconos-que-hacemos">
                	<?php
                    $term_i = 1;
                    $terms = get_terms( array(
                        'taxonomy' => 'que_hacemos',
                        'hide_empty' => 0
                    ));
                    foreach ($terms as $term){
                        //echo '<li><a href="'.get_term_link($term->term_id).'">'.$term->name.'</a></li>';
                        ?>
                	    <div class="col-md-2">
                	    	<a href="<?php echo get_term_link($term->term_id) ?>" class="<?php echo $term->slug; ?>">
                    	    	<?php echo $term->name; ?>
                            </a>
                	    </div><!-- .col-md-3 -->
                        <?php  
                        $term_i++;
                    }
                    ?>            	
            </div><!-- .row -->
            
        
        <?php 
        global $wp_query;
        $args = array_merge( $wp_query->query_vars, array( 'orderby' => 'menu_order',
                                                            'order'  => 'asc'
                                                    )
        );
        query_posts( $args );
            if ( have_posts() ) { 
            $i = 1;
            while ( have_posts() ) { 
                the_post();
                if ($i == 1){
                    echo '<div class="row">';
                }
                
            ?>
                <div class="col-md-4 col-sm-6 snippet noticias alto-fijo">
                    <?php 
                    if(has_post_thumbnail($the_query->ID)){ ?>
                        <p class="foto">
                            <a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail('ficha', array('class' => 'img-responsive')); ?></a>
                        </p>
                    <?php 
                    }
                    ?>                    
            		<div class="info">
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <p class="martel"><?php echo get_the_excerpt(__('(more…)')); ?></p>
                    </div>
                </div><!-- .col-md-4 -->
            <?php 
                $i++;
                if ($i == 4){
                    $i = 1;
                    echo '</div>';
                }
                
            } wp_reset_postdata(); 
            if ($i > 3){
                echo '</div>';
            }
        } else {
            ?>
            <div class="col-md-12">
                <div class="banda_de_texto">
                <h2><?php echo __('Sin resultados', 'ETG_text_domain'); ?></h2>
                <p><?php echo __('Todavía no hay datos.', 'ETG_text_domain'); ?></p>
                </div>
            </div><!-- .col-md-12 -->
            <?php
        }
        ?>
        
        
    <div class="row">
        <div class="col-md-12">
        	<p class="text-center">
            <?php 
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => $wp_query->max_num_pages,
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => sprintf( '<i></i> %1$s', __( 'Anterior', 'text-domain' ) ),
                    'next_text'    => sprintf( '%1$s <i></i>', __( 'Siguiente', 'text-domain' ) ),
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
            ?>
            </p>
        </div><!-- .col-md-12 -->
    </div><!-- .row -->
        
        
        
            <div class="row" style="padding-top: 20px; padding-bottom: 40px;">
                <div class="col-md-8 col-md-offset-2">
                	<?php echo nl2br($descripcion_seccion); ?>
                </div><!-- .col-md-4 -->
            </div>
        
        
	</article>
</div>
	
<?php get_footer(); ?>