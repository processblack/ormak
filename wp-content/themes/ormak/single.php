<?php get_header(); ?>

    <?php 
        ETG_cabecera(get_the_ID());
    ?>


<article id="contenido" class="home">
    <div class="container">
        <div class="row">
        <?php 
        if ( have_posts() ) { 
            $i = 0;
            while ( have_posts() ) { 
                the_post();
            ?>
                <div class="col-md-8 col-md-offset-2">
                    <div class="post">
                            
                		<div class="info" style="padding: 50px 0;">
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <p class="fecha"><?php echo get_the_date('d\/m\/Y'); ?></p>
                            <p class="martel"><?php echo the_content(); ?></p>
                        </div>
                    </div>
                    
                </div><!-- .col-md-8 -->
            <?php 
                $i++;
            } 
        } wp_reset_postdata(); ?>
    </div><!-- row -->
	</article>
</div>
	
<?php #get_sidebar(); ?>
<?php get_footer(); ?>
