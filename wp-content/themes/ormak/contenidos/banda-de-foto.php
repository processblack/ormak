<?php
    $foto = get_sub_field('fotografia');
    #print_r($foto);
?>
<div class="banda_de_foto">
    <div class="container">
    	<div class="row">
    	    <div class="col-sm-12">
        	    <div class="imagen">
    			    <div class="corte <?php echo get_sub_field('corte'); ?>"></div>
    			    <div class="forma <?php echo get_sub_field('corte'); ?>"></div>
                    <img src="<?php echo $foto['sizes']['slide']; ?>" alt="<?php echo get_sub_field('titular_fotografia'); ?>" class="img-responsive" />
                    <div class="caption">
                        <?php if (get_sub_field('texto') !== "") { ?>
                        <h3 class="titular"><?php echo get_sub_field('texto'); ?></h3>
                        <?php } ?>
                    </div>
                </div>
    	    </div><!-- .col-sm-12 -->
    	</div><!-- .row -->
    </div><!-- container -->
</div>