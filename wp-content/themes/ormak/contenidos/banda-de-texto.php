<?php $params = get_sub_field_object('texto'); echo '<!--'; print_r($params); echo '-->'; ?>
<div id="banda_de_texto_<?php echo $params['ID']; ?>" class="banda_de_texto">
	<div class="container">
		<div class="row">
		    <div class="col-md-6">
                <?php the_sub_field('texto'); ?>
		    </div><!-- .col-md-6 -->
            <?php if (get_the_ID() == 4 and $params['name'] == 'crear_contenido_1_texto'){ ?>
		    <div class="col-md-6 boton-descarga">
                <div class="text-center">
                    <h3 class="light text-uppercase" style="margin-bottom: 20px;"><?php echo __('Si quieres saber más...', 'ETG_text_domain'); ?></h3>
                    <p><a href="/descargas/construcciones-ormak-catalogo.pdf" target="_blank" class="btn btn-primary text-uppercase"><?php echo __('Descargar catálogo', 'ETG_text_domain'); ?></a></p>
                </div>
		    </div><!-- .col-md-6 -->            
            <?php } ?>
		</div><!-- .row -->
	</div><!-- .container -->
</div>
