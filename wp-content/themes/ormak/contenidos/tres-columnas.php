<div class="banda_tres_columnas">
	<div class="container">
		<div class="row">
		    <div class="col-md-4">                
                <?php $imagen = get_sub_field('imagen_1');?>
                <img src="<?php echo $imagen['url']; ?>" alt="<?php the_sub_field('titular_1'); ?>" />
                <h3><?php the_sub_field('titular_1'); ?></h3>
                <?php the_sub_field('texto_1'); ?>
		    </div><!-- .col-md-6 -->
		    <div class="col-md-4">                
                <?php $imagen = get_sub_field('imagen_2');?>
                <img src="<?php echo $imagen['url']; ?>" alt="<?php the_sub_field('titular_s'); ?>" />
                <h3><?php the_sub_field('titular_2'); ?></h3>
                <?php the_sub_field('texto_2'); ?>
		    </div><!-- .col-md-6 -->
		    <div class="col-md-4">                
                <?php $imagen = get_sub_field('imagen_3');?>
                <img src="<?php echo $imagen['url']; ?>" alt="<?php the_sub_field('titular_3'); ?>" />
                <h3><?php the_sub_field('titular_3'); ?></h3>
                <?php the_sub_field('texto_3'); ?>
		    </div><!-- .col-md-6 -->
		</div><!-- .row -->
	</div><!-- .container -->
</div>
