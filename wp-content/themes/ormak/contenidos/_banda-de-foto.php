<?php
    $foto = get_sub_field('fotografia');
?>
	<div class="banda_de_foto" style="background-image: url(<?php echo $foto['sizes']['slide']; ?>)">
    	<div class="color"></div>
    	<div class="container">                    		
        	<div class="row">
        	    <div class="col-md-7<?php if (get_sub_field('derecha') == true) { echo ' col-md-offset-5'; } ?>">
                    <?php the_sub_field('texto'); ?>
        	    </div><!-- .col-md-7 -->
        	</div><!-- .row -->
    	</div><!-- .container -->
    </div>
<?php
