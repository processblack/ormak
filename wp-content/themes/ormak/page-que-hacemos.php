<?php 
    // Template Name: Qué hacemos
    get_header();
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <article id="contenido">
        <?php
        if( have_rows('crear_contenido') ):
            while ( have_rows('crear_contenido') ) : the_row();
                if( get_row_layout() == 'banda_de_foto' ):
                    get_template_part('contenidos/banda-de-foto');
                
                elseif( get_row_layout() == 'banda_de_texto' ): 
                    get_template_part('contenidos/banda-de-texto');

                elseif( get_row_layout() == 'tres_columnas' ): 
                    get_template_part('contenidos/tres-columnas');
                    
                endif;
            endwhile;
        endif;
        ?>
            <div class="container">
<!--             		<h2 class="titular"><?php the_title();?></h2> -->
<!--                 		<?php the_content();?>                	 -->
                <div class="row">
                    <div class="col-md-4">
                    	<h4 class="light"><?php echo __('Hacemos<br />obras de:', 'ETG_text_domain'); ?></h4>
                    </div><!-- .col-md-4 -->
                    <div class="col-md-8">
                    	<h4 class="light"><?php echo __('En:', 'ETG_text_domain'); ?></h4>
                    </div><!-- .col-md-8 -->
                </div><!-- .row -->
                <div class="row">
                    <div class="col-md-4">
                    	<ul class="puntitos">
                        	<?php
                            $terms = get_terms( array(
                                'taxonomy' => 'como_lo_hacemos',
                                'hide_empty' => 0
                            ));
                            foreach ($terms as $term){
                                echo '<li><a href="'.get_term_link($term->term_id).'">'.$term->name.'</a></li>';
                            }
                            ?>
                    	</ul>
                    </div><!-- .col-md-4 -->
                    <div class="col-md-4">
                    	<ul class="puntitos">
                        	<?php
                            $terms = get_terms( array(
                                'taxonomy' => 'que_hacemos',
                                'hide_empty' => 0,
                                'number' => 3,
                                'offset' => 0
                            ));
                            foreach ($terms as $term){
                                echo '<li><a href="'.get_term_link($term->term_id).'">'.$term->name.'</a></li>';
                            }
                            ?>
                    	</ul>
                    </div><!-- .col-md-4 -->
                    <div class="col-md-4">
                    	<ul class="puntitos">
                        	<?php
                            $terms = get_terms( array(
                                'taxonomy' => 'que_hacemos',
                                'hide_empty' => 0,
                                'number' => 3,
                                'offset' => 3
                            ));
                            foreach ($terms as $term){
                                echo '<li><a href="'.get_term_link($term->term_id).'">'.$term->name.'</a></li>';
                            }
                            ?>
                    	</ul>
                    </div><!-- .col-md-4 -->
            	</div><!-- .row -->
            	<div class="row pd50">
            	    <div class="col-md-12">
            	    	<img src="<?php bloginfo( 'template_url' ); ?>/img/empresas.png" alt="empresas" class="img-responsive" />
            	    </div><!-- .col-md-12 -->
            	</div><!-- .row -->
            </div><!-- .container -->
    </article>
<?php endwhile; ?>

<?php get_footer(); ?>