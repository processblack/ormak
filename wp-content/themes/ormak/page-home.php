<?php 
    // Template Name: Página de home
    get_header();
?>
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<article id="contenido" class="inicio">

    <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 pd50">
                   <h2 class="text-uppercase text-center text-light"><?php echo __('Tu empresa constructora de confianza en Gipuzkoa.', 'ETG_text_domain'); ?></h2>
                </div><!-- .col-md-12 -->
            </div>
        </div><!-- .container -->
    

    <div id="noticias" class="home">
            <div class="container">
                <!-- <div class="row">
                    <div class="col-md-12">
                        <h1><?php echo __('Últimas noticias', 'ETG_text_domain'); ?></h1>
                    </div>
                </div> -->
                
                    
        
                    
                    <div class="cycle-slideshow relativiza" 
                        data-cycle-fx="fade" 
                        data-cycle-timeout="3000"
                        data-cycle-slides=".row"
                        data-cycle-log="false"
                        data-cycle-pause-on-hover="true"
                        data-cycle-pager="#bullets"
                        data-cycle-pager-template="<strong><a href=#><span>{{slideNum}}<span></a> </strong>"
                        >
                            <?php
                            $i = 0;
                            $args = array( 'posts_per_page' => 10 );
                            $the_query = new WP_Query( $args );
                            if ( $the_query->have_posts() ) {
                                while ( $the_query->have_posts() ) : $the_query->the_post();
        
        
                        //if ($i == 0){
                            echo ' <div class="row" style="margin-top: 30px; margin-bottom: 30px">';
                        //}
                    ?>
                    <div class="col-md-12">
                        <a href="<?php the_permalink(); ?>" class="snippet-noticias">
                            <div class="foto">
                                <div class="<?php echo ($i % 2 !== 0) ? 'corte-derecha-arriba' : 'corte-izquierda-arriba'; ?>"></div>
                                <img src="<?php the_post_thumbnail_url( 'slide' ); ?>" srcset="<?php echo wp_get_attachment_image_sizes('slide'); ?>" alt="" class="img-responsive" />
                            </div>
                            <h2 class="text-uppercase"><?php the_title(); ?></h2>
                            <p class="fecha"><?php echo get_the_date('d/m/Y'); ?></p>
                            
        <!--
                                        <p>
                                            
                                            <?php 
                                                foreach((get_the_category()) as $category) { 
                                                    echo $category->cat_name;
                                                } 
                                            ?>
                                        </p>
        -->
                            
                        </a>
                    </div><!-- .col-md-6 -->
                    <?php 
                        //if ($i == 1) { 
                            echo '</div> ';
                        //    $i = 0;
                        //}else {
                        //    $i++;
                        //}
        
        
                    
                                endwhile;
                                wp_reset_postdata();
                            }
                if ($i == 1){
                    echo '</div> ';
                }
                ?>
                <div class="cycle-prev"></div>
                <div class="cycle-next"></div>
                <?php
                    echo '</div> ';        
                            ?>
                    
                
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center"><a href="<?php bloginfo('url');?>/actualidad" class="btn btn-lg btn-primary"><?php echo __('Leer más noticias', 'ETG_text_domain'); ?></a></p>
                    </div>
                </div>
                
            </div><!-- .container -->
        </div>
	
	
		
	<div class="fondo-color">
    	<div class="container">
        	<div class="row">
        	    <div class="col-sm-12">
        	    	<div id="slide">
                        <div id="slide_home" class="cycle-slideshow" 
                            data-cycle-fx="fade" 
                            data-cycle-timeout="3000"
                            data-cycle-slides="> div"
                            data-cycle-log="false"
                            data-cycle-pause-on-hover="true"
                            data-cycle-pager="#bullets"
                            data-cycle-pager-template="<strong><a href=#><span>{{slideNum}}<span></a> </strong>"
                            >
                                
                			<?php 
                    		foreach (get_field('slides') as $slide){
                            ?>
                			<div class="slide">
                                <a href="<?php echo $slide['link']; ?>" class="item">
                    			    <div class="corte <?php echo $slide['corte']; ?>"></div>
                    			    <div class="forma <?php echo $slide['corte']; ?>"></div>
                                    <img src="<?php echo $slide['fotografia']['sizes']['slide']; ?>" alt="<?php echo $slide['titular_fotografia']; ?>" class="img-responsive" /><div class="caption">
                                        <?php if ($slide['titular'] !== "" or $slide['texto'] !== "") { ?>
                                        <h3 class="titular"><?php echo $slide['titular']; ?></h3>
                                        <?php } ?>
                                    </div>
                                </a>
                            </div>
                    		<?php 
                            }	
                            ?>         
                        </div><!-- #slide_home -->      
                         	
                    </div>
        	    </div><!-- .col-sm-12 -->
        	</div><!-- .row -->
        </div><!-- container -->
    </div>	
    
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
            	<div id="bullets" class="clearfix"></div>
            </div><!-- .col-sm-12 -->
        </div><!-- .row -->
    	<div class="row">
    	    <div class="col-md-12 pd50">
        	   <h2 class=" text-center text-light"><?php echo __('GRAN EXPERIENCIA EN CONSTRUCCIÓN DE OBRAS INDUSTRIALES, DE EDIFICACIÓN Y REFORMAS INTEGRALES DE EDIFICIOS EN CASCO URBANO', 'ETG_text_domain'); ?></h2>
    	    </div><!-- .col-md-12 -->
    	</div><!-- .row -->
    </div><!-- container -->
</article>
<!--     <div style="background:#C2F5F9; padding: 20px 0;"> -->




    
    
    <div id="destacados">
        <div class="container">
                
            <div class="row">
                <div class="col-sm-4">
                	<div class="snippet">
                    	<?php
                        $foto = get_field('fotografia_1');
                        ?>
                        <a href="<?php echo get_field('link_1'); ?>">
                            <img src="<?php echo $foto['sizes']['ficha']; ?>" alt="<?php echo get_field('titular_1'); ?>" class="img-responsive">
                            <span></span>
                            <h2><?php echo get_field('titular_1'); ?></h2>
                        </a>
                    </div>
                	<div style="margin-top: 20px"><?php echo get_field('texto_1'); ?></div>
                </div>
                <div class="col-sm-4">
                	<div class="snippet">
                    	<?php
                        $foto = get_field('fotografia_2');
                        ?>
                        <a href="<?php echo get_field('link_2'); ?>">
                            <img src="<?php echo $foto['sizes']['ficha']; ?>" alt="<?php echo get_field('titular_2'); ?>" class="img-responsive">
                            <span></span>
                            <h2><?php echo get_field('titular_2'); ?></h2>
                        </a>
                    </div>
                	<div style="margin-top: 20px"><?php echo get_field('texto_2'); ?></div>
                </div><!-- .col-sm-4 -->
                <div class="col-sm-4">
                	<div class="snippet">
                    	<?php
                        $foto = get_field('fotografia_3');
                        ?>
                        <a href="<?php echo get_field('link_3'); ?>">
                            <img src="<?php echo $foto['sizes']['ficha']; ?>" alt="<?php echo get_field('titular_3'); ?>" class="img-responsive">
                            <span></span>
                            <h2><?php echo get_field('titular_3'); ?></h2>
                        </a>
                    </div>
                	<div style="margin-top: 20px"><?php echo get_field('texto_3'); ?></div>
                </div>
            </div>
            
            <div class="row">
        	    <div class="col-md-12 pd50">
        	    	<h2 class="text-uppercase text-center text-light"><?php echo __('Construyas lo que construyas,<br />construimos contigo', 'ETG_text_domain'); ?></h2>
        	    </div>
        	</div> 
        </div><!-- container -->
    </div><!-- #destacados -->

	<?php endwhile; ?>
<?php get_footer(); ?>